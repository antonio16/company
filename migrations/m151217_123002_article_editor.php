<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_123002_article_editor extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%article_editor}}', [
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
        ]);

        $this->addPrimaryKey(
            'pk_article_editor',
            '{{%article_editor}}',
            ['company_id', 'user_id']
        );

        $this->addForeignKey(
            'fk_article_editor_company',
            '{{%article_editor}}',
            'company_id',
            '{{%company}}',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk_article_editor_user',
            '{{%article_editor}}',
            'user_id',
            '{{%user}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_article_editor_company', '{{%article_editor}}');
        $this->dropForeignKey('fk_article_editor_user', '{{%article_editor}}');

        $this->dropTable('{{%article_editor}}');
    }
}
