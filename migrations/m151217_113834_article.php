<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_113834_article extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id' => 'pk',
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'text' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);

        $this->addForeignKey(
            'fk_article_company',
            '{{%article}}',
            'company_id',
            '{{%company}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_article_company', '{{%article}}');

        $this->dropTable('{{%article}}');
    }
}
