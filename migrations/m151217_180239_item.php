<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_180239_item extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%item}}', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'group_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);

        $this->addForeignKey(
            'fk_item_group',
            '{{%item}}',
            'group_id',
            '{{%group}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_item_group', '{{%item}}');

        $this->dropTable('{{%item}}');
    }
}
