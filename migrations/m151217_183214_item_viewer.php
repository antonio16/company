<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_183214_item_viewer extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%item_viewer}}', [
            'group_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
        ]);

        $this->addPrimaryKey(
            'pk_item_viewer',
            '{{%item_viewer}}',
            ['group_id', 'user_id']
        );

        $this->addForeignKey(
            'fk_item_viewer_group',
            '{{%item_viewer}}',
            'group_id',
            '{{%group}}',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk_item_viewer_user',
            '{{%item_viewer}}',
            'user_id',
            '{{%user}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_item_viewer_group', '{{%item_viewer}}');
        $this->dropForeignKey('fk_item_viewer_user', '{{%item_viewer}}');

        $this->dropTable('{{%item_viewer}}');
    }
}
