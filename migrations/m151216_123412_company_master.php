<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_123412_company_master extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%company_master}}', [
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
        ]);

        $this->addPrimaryKey(
            'pk_company_master',
            '{{%company_master}}',
            ['company_id', 'user_id']
        );

        $this->addForeignKey(
            'fk_company_master_company',
            '{{%company_master}}',
            'company_id',
            '{{%company}}',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk_company_master_user',
            '{{%company_master}}',
            'user_id',
            '{{%user}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_company_master_company', '{{%company_master}}');
        $this->dropForeignKey('fk_company_master_user', '{{%company_master}}');

        $this->dropTable('{{%company_master}}');
    }
}
