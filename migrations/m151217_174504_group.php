<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_174504_group extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%group}}', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);

        $this->addForeignKey(
            'fk_group_company',
            '{{%group}}',
            'company_id',
            '{{%company}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_group_company', '{{%group}}');

        $this->dropTable('{{%group}}');
    }
}
