<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_105939_company extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%company}}', [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'owner_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);

        $this->addForeignKey(
            'fk_company_owner',
            '{{%company}}',
            'owner_id',
            '{{%user}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_company_owner', '{{%company}}');

        $this->dropTable('{{%company}}');
    }
}
