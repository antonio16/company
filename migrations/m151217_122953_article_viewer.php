<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_122953_article_viewer extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%article_viewer}}', [
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
        ]);

        $this->addPrimaryKey(
            'pk_article_viewer',
            '{{%article_viewer}}',
            ['company_id', 'user_id']
        );

        $this->addForeignKey(
            'fk_article_viewer_company',
            '{{%article_viewer}}',
            'company_id',
            '{{%company}}',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk_article_viewer_user',
            '{{%article_viewer}}',
            'user_id',
            '{{%user}}',
            'id',
            'cascade',
            'cascade'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_article_viewer_company', '{{%article_viewer}}');
        $this->dropForeignKey('fk_article_viewer_user', '{{%article_viewer}}');

        $this->dropTable('{{%article_viewer}}');
    }
}
