<?php

namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionIndex() {

        //TODO
        //echo "\nPlease edit actionIndex() to do something.\n\n";

        $this->_initAdmin();
        $this->_initUser();
        $this->_initOwner();
        $this->_initMaster();

        $this->_initCompanyPermissions();

        $this->_initArticlePermissions();
        $this->_initArticlePermissionsGranted();

        $this->_initGroupItemPermissions();
    }

    public function actionGrantAdmin($username) {
        $user = User::findByUsername($username);

        if ($user === null) {
            echo "\nUser " . $username . " not found.\n\n";
        } else {
            $auth = Yii::$app->authManager;
            $adminRole = $auth->createRole('admin');
            $auth->assign($adminRole, $user->id);

            echo "\nAdmin role granted to user " . $username . "\n\n";
        }
    }

    /**
     * Initializes admin role
     */
    private function _initAdmin() {
        $auth = Yii::$app->authManager;

        $adminRole = $auth->createRole('admin');
        $auth->add($adminRole);

        echo "\nAdmin role installed";
    }

    /**
     * Initializes user role
     */
    private function _initUser() {
        $auth = Yii::$app->authManager;

        $userRole = $auth->createRole('user');
        $auth->add($userRole);

        echo "\User role installed";
    }

    /**
     * Initializes owner role
     */
    private function _initOwner() {
        $auth = Yii::$app->authManager;

        $rule = new \app\rbac\OwnerRule();
        $auth->add($rule);

        $owner = $auth->createRole('owner');
        $owner->description = 'Company owner role';
        $owner->ruleName = $rule->name;
        $auth->add($owner);

        $user = $auth->getRole('user');
        $auth->addChild($user, $owner);

        echo "\nOwner role installed";
    }

    /**
     * Initializes master role
     */
    private function _initMaster() {
        $auth = Yii::$app->authManager;

        $rule = new \app\rbac\MasterRule();
        $auth->add($rule);

        $master = $auth->createRole('master');
        $master->description = 'Company master role';
        $master->ruleName = $rule->name;
        $auth->add($master);

        $owner = $auth->getRole('owner');
        $auth->addChild($owner, $master);

        echo "\nMaster role installed";
    }

    /**
     * Initializes company permissions
     */
    private function _initCompanyPermissions() {
        $auth = Yii::$app->authManager;

        // creating permissions
        $view = $auth->createPermission('viewCompany');
        $update = $auth->createPermission('updateCompany');
        $delete = $auth->createPermission('deleteCompany');
        $addMaster = $auth->createPermission('addMaster');
        $deleteMaster = $auth->createPermission('deleteMaster');

        // adding permissions to db
        $auth->add($view);
        $auth->add($update);
        $auth->add($delete);
        $auth->add($addMaster);
        $auth->add($deleteMaster);

        // assigning all permissions to admins
        $adminRole = $auth->getRole('admin');
        $auth->addChild($adminRole, $view);
        $auth->addChild($adminRole, $update);
        $auth->addChild($adminRole, $delete);
        $auth->addChild($adminRole, $addMaster);
        $auth->addChild($adminRole, $deleteMaster);

        // assigning all permissions to owners
        $ownerRole = $auth->getRole('owner');
        $auth->addChild($ownerRole, $view);
        $auth->addChild($ownerRole, $update);
        $auth->addChild($ownerRole, $delete);
        $auth->addChild($ownerRole, $addMaster);
        $auth->addChild($ownerRole, $deleteMaster);

        // assigning some permissions to masters
        $masterRole = $auth->getRole('master');
        $auth->addChild($masterRole, $view);
        $auth->addChild($masterRole, $update);

        echo "\nCompany permissions installed";
    }

    private function _initArticlePermissions() {
        $auth = Yii::$app->authManager;

        // creating permissions
        $view = $auth->createPermission('viewArticles');
        $edit = $auth->createPermission('editArticles');

        // adding permissions to db
        $auth->add($view);
        $auth->add($edit);

        // assigning all permissions to admins
        $adminRole = $auth->getRole('admin');
        $auth->addChild($adminRole, $view);
        $auth->addChild($adminRole, $edit);

        // assigning all permissions to owners
        $ownerRole = $auth->getRole('owner');
        $auth->addChild($ownerRole, $view);
        $auth->addChild($ownerRole, $edit);

        // assigning all permissions to masters
        $masterRole = $auth->getRole('master');
        $auth->addChild($masterRole, $view);
        $auth->addChild($masterRole, $edit);

        echo "\nArticle permissions installed";
    }

    private function _initArticlePermissionsGranted() {
        $auth = Yii::$app->authManager;

        // creating permissions
        $viewGranted = $auth->createPermission('viewArticlesGranted');
        $editGranted = $auth->createPermission('editArticlesGranted');

        // creating and adding rules
        $viewRule = new \app\rbac\ViewArticleRule();
        $editRule = new \app\rbac\EditArticleRule();
        $auth->add($viewRule);
        $auth->add($editRule);

        // assigning rules to permissions
        $viewGranted->ruleName = $viewRule->name;
        $editGranted->ruleName = $editRule->name;

        // adding permissions to db
        $auth->add($viewGranted);
        $auth->add($editGranted);

        // getting common permissions
        $view = $auth->getPermission('viewArticles');
        $edit = $auth->getPermission('editArticles');

        // getting user role
        $userRole = $auth->getRole('user');

        // assigning granted permissions to user role
        $auth->addChild($userRole, $viewGranted);
        $auth->addChild($userRole, $editGranted);

        // assigning common permissions to granted permissions
        $auth->addChild($viewGranted, $view);
        $auth->addChild($editGranted, $edit);

        echo "\nGranted article permissions installed";
    }

    private function _initGroupItemPermissions() {
        $auth = Yii::$app->authManager;

        // creating permissions
        $itemView = $auth->createPermission('viewItems');
        $itemViewGranted = $auth->createPermission('viewItemsGranted');

        // creating and adding rule
        $viewItemsRule = new \app\rbac\ViewItemsRule();
        $auth->add($viewItemsRule);

        // assigning rule to permission
        $itemViewGranted->ruleName = $viewItemsRule->name;

        // adding permissions to db
        $auth->add($itemView);
        $auth->add($itemViewGranted);

        // getting roles
        $adminRole = $auth->getRole('admin');
        $ownerRole = $auth->getRole('owner');
        $masterRole = $auth->getRole('master');
        $userRole = $auth->getRole('user');

        // assigning children
        $auth->addChild($adminRole, $itemView);
        $auth->addChild($ownerRole, $itemView);
        $auth->addChild($masterRole, $itemView);
        $auth->addChild($userRole, $itemViewGranted);
        $auth->addChild($itemViewGranted, $itemView);

        echo "\nItem group permissions installed\n\n";
    }
}