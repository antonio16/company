<?php

namespace app\controllers;

use app\models\Company;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

class CompanyController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        Yii::$app->user->returnUrl =
            Yii::$app->urlManager->createUrl('company/index');

        $companyProvider = new ActiveDataProvider([
            'query' => Company::find(),
        ]);
        
        return $this->render('index', [
            'companyProvider' => $companyProvider
        ]);
    }

    public function actionCreate() {
        $model = new Company();

        if ($model->load(Yii::$app->request->post())) {
            $model->owner_id = Yii::$app->user->id;
            if ($model->save()) {
                return $this->goBack();
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($id) {
        //$this->checkAccess($id, 'viewCompany');

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        $masterProvider = new ActiveDataProvider([
            'query' => $company->getMasters()
        ]);

        return $this->render('view', [
            'company' => $company,
            'masterProvider' => $masterProvider
        ]);
    }

    public function actionUpdate($id) {
        $this->checkAccess($id, 'updateCompany');

        /**
         * @var $model Company
         */
        $model = Company::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->goBack();
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $this->checkAccess($id, 'deleteCompany');

        /**
         * @var $model Company
         */
        $model = Company::findOne($id);

        $model->delete();

        return $this->goBack();
    }

    /*public function actionAddMaster($id) {
        $this->checkAccess($id, 'addMaster');

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $companyMaster = new CompanyMaster();
            $companyMaster->company_id = $post['company_id'];
            $companyMaster->user_id = $post['user_id'];

            if ($companyMaster->save()) {
                return $this->redirect(['/company/view', 'id' => $id]);
            }
        }


        $company = Company::findOne($id);

        $userProvider = new ActiveDataProvider([
            'query' => User::find()
        ]);

        return $this->render('add-master', [
            'company' => $company,
            'userProvider' => $userProvider,
        ]);
    }*/

    /*public function actionDeleteMaster($id) {
        $this->checkAccess($id, 'deleteMaster');

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            CompanyMaster::deleteAll([
                'company_id' => $post['company_id'],
                'user_id' => $post['user_id'],
            ]);

        }

        return $this->redirect(['/company/view', 'id' => $id]);
    }*/
}