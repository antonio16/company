<?php

namespace app\controllers;

use app\models\Item;
use Yii;
use app\models\Group;
use yii\base\Exception;
use yii\data\ActiveDataProvider;

class ItemController extends Controller
{

    public function actionIndex($id) {

        Yii::$app->user->returnUrl =
            Yii::$app->urlManager->createUrl(['item/index', 'id' => $id]);

        /**
         * @var $group Group
         */
        $group = Group::findOne($id);

        $groupProvider = new ActiveDataProvider([
            'query' => $group->getItems()
        ]);

        return $this->render('index', [
            'group' => $group,
            'itemProvider' => $groupProvider
        ]);
    }

    public function actionCreate($id) {
        /**
         * @var $group Group
         */
        $group = Group::findOne($id);

        $this->checkAccess($group->company_id, 'viewCompany');

        $model = new Item();

        if ($model->load(Yii::$app->request->post())) {
            $model->group_id = $id;
            if ($model->save()) {
                return $this->goBack();
            }
        }

        return $this->render('create', [
            'model' => $model,
            'group' => $group,
        ]);
    }

    public function actionView($id) {
        /**
         * @var $item Item
         */
        $item = Item::findOne($id);

        try {
            $this->checkAccess($item->group_id, 'viewItems');
        } catch (Exception $e) {
            $this->checkAccess($item->group->company_id, 'viewItems');
        }

        return $this->render('view', [
            'item' => $item,
        ]);
    }

    public function actionUpdate($id) {
        /**
         * @var $model Item
         */
        $model = Item::findOne($id);

        $group = $model->group;

        $this->checkAccess($group->company_id, 'viewCompany');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->goBack();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'group' => $group,
        ]);
    }

    public function actionDelete($id) {
        /**
         * @var $model Item
         */
        $model = Item::findOne($id);

        $this->checkAccess($model->group->company_id, 'viewCompany');


        $model->delete();

        return $this->goBack();
    }
}