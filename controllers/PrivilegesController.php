<?php

namespace app\controllers;

use app\models\ArticleEditor;
use app\models\ArticleViewer;
use app\models\Company;
use app\models\GrantForm;
use app\models\ItemViewer;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

class PrivilegesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => ['class' => 'app\rbac\AccessRule'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'owner', 'master'],
                        'params' => [
                            'company_id' => Yii::$app->request->get('id'),
                        ]
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($id) {
        //$this->checkAccess($id, 'viewCompany');

        Yii::$app->user->returnUrl =
            Yii::$app->urlManager->createUrl(['privileges/index', 'id' => $id]);

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        $viewerQuery = ArticleViewer::find()->select('user_id')
            ->where(['company_id' => $id]);
        $editorQuery = ArticleEditor::find()->select('user_id')
            ->where(['company_id' => $id]);

        $itemViewerQuery = ItemViewer::find()->select('user_id')
            ->innerJoinWith('group')->where(['company_id' => $id]);

        $query = $viewerQuery->union($editorQuery)
            ->union($itemViewerQuery)->groupBy('user_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index', [
            'company' => $company,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFind($id) {
        //$this->checkAccess($id, 'viewCompany');

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        $userProvider = new ActiveDataProvider([
            'query' => User::find()
        ]);

        return $this->render('find', [
            'company' => $company,
            'userProvider' => $userProvider,
        ]);
    }

    public function actionGrant($id, $user_id) {
        //$this->checkAccess($id, 'viewCompany');

        $model = GrantForm::create($id, $user_id);

        if ($model->load(Yii::$app->request->post())) {

            $model->grant();
            $this->goBack();
        }

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        $groupArray = [];
        foreach ($company->groups as $group) {
            $groupArray[$group->id] = $group->name;
        }

        /**
         * @var $user User
         */
        $user = User::findOne($user_id);

        return $this->render('grant', [
            'model' => $model,
            'company' => $company,
            'user' => $user,
            'groupArray' => $groupArray,
        ]);
    }
}