<?php

namespace app\controllers;

use Yii;
use yii\web\ForbiddenHttpException;

class Controller extends \yii\web\Controller
{
    protected function checkAccess($companyId, $permissionName) {
        if (!Yii::$app->user->can($permissionName, ['company_id' => $companyId])) {
            throw new ForbiddenHttpException(
                'You are not allowed to perform this action.');
        }
    }
}