<?php

namespace app\controllers;

use Yii;
use app\models\Company;
use app\models\CompanyMaster;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

class MasterController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => ['class' => 'app\rbac\AccessRule'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'owner'],
                        'params' => [
                            'company_id' => Yii::$app->request->get('id'),
                        ]
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($id) {
        //$this->checkAccess($id, 'viewCompany');

        Yii::$app->user->returnUrl =
            Yii::$app->urlManager->createUrl(['master/index', 'id' => $id]);

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        $masterProvider = new ActiveDataProvider([
            'query' => $company->getMasters()
        ]);

        return $this->render('index', [
            'company' => $company,
            'masterProvider' => $masterProvider
        ]);
    }

    public function actionAdd($id) {
        //$this->checkAccess($id, 'addMaster');

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $companyMaster = new CompanyMaster();
            $companyMaster->company_id = $post['company_id'];
            $companyMaster->user_id = $post['user_id'];

            if ($companyMaster->save()) {
                return $this->goBack();
            }
        }

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        $userProvider = new ActiveDataProvider([
            'query' => User::find()
        ]);

        return $this->render('add', [
            'company' => $company,
            'userProvider' => $userProvider,
        ]);
    }

    public function actionDelete($id) {
        //$this->checkAccess($id, 'deleteMaster');

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            CompanyMaster::deleteAll([
                'company_id' => $post['company_id'],
                'user_id' => $post['user_id'],
            ]);

        }

        return $this->goBack();
    }
}