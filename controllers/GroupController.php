<?php

namespace app\controllers;

use app\models\Group;
use Yii;
use app\models\Company;
use yii\data\ActiveDataProvider;

class GroupController extends Controller
{

    public function actionIndex($id) {

        Yii::$app->user->returnUrl =
            Yii::$app->urlManager->createUrl(['group/index', 'id' => $id]);

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        $groupProvider = new ActiveDataProvider([
            'query' => $company->getGroups()
        ]);

        return $this->render('index', [
            'company' => $company,
            'groupProvider' => $groupProvider
        ]);
    }

    public function actionCreate($id) {
        $this->checkAccess($id, 'viewCompany');

        $model = new Group();

        if ($model->load(Yii::$app->request->post())) {
            $model->company_id = $id;
            if ($model->save()) {
                return $this->goBack();
            }
        }

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    public function actionView($id) {
        return $this->redirect(['item/index', 'id' => $id]);
    }


    public function actionUpdate($id) {
        /**
         * @var $model Group
         */
        $model = Group::findOne($id);

        $this->checkAccess($model->company_id, 'viewCompany');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->goBack();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'company' => $model->company,
        ]);
    }

    public function actionDelete($id) {
        /**
         * @var $model Group
         */
        $model = Group::findOne($id);

        $this->checkAccess($model->company_id, 'viewCompany');


        $model->delete();

        return $this->goBack();
    }
}