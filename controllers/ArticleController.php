<?php

namespace app\controllers;

use app\models\Article;
use Yii;
use yii\data\ActiveDataProvider;
use app\models\Company;

class ArticleController extends Controller
{
    public function actionIndex($id) {

        Yii::$app->user->returnUrl =
            Yii::$app->urlManager->createUrl(['article/index', 'id' => $id]);

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        $articleProvider = new ActiveDataProvider([
            'query' => $company->getArticles()
        ]);

        return $this->render('index', [
            'company' => $company,
            'articleProvider' => $articleProvider
        ]);
    }

    public function actionCreate($id) {
        $this->checkAccess($id, 'editArticles');

        $model = new Article();

        if ($model->load(Yii::$app->request->post())) {
            $model->company_id = $id;
            if ($model->save()) {
                return $this->goBack();
            }
        }

        /**
         * @var $company Company
         */
        $company = Company::findOne($id);

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    public function actionView($id) {
        /**
         * @var $article Article
         */
        $article = Article::findOne($id);

        $this->checkAccess($article->company_id, 'viewArticles');

        return $this->render('view', [
            'article' => $article,
            'company' => $article->company,
        ]);
    }

    public function actionUpdate($id) {
        /**
         * @var $model Article
         */
        $model = Article::findOne($id);

        $this->checkAccess($model->company_id, 'editArticles');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->goBack();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'company' => $model->company,
        ]);
    }

    public function actionDelete($id) {
        /**
         * @var $model Article
         */
        $model = Article::findOne($id);

        $this->checkAccess($model->company_id, 'editArticles');


        $model->delete();

        return $this->goBack();
    }
}