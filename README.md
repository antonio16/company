COMPANY APP
============================


INSTALLATION
-------------------

### Enter following commands

~~~
cd /path/to/project
git clone https://antonio16@bitbucket.org/antonio16/company.git
composer update
./yii migrate --migrationPath=@yii\rbac\migrations
./yii migrate
./yii rbac
~~~

If there are issues on your server with hiding `index.php`, please enable `showScriptName` option in config. 

GRATING ADMIN ROLES
------------

To grant admin role for user `administrator123` enter in command line:

~~~
./yii rbac/grant-admin administrator123
~~~