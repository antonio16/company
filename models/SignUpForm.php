<?php

namespace app\models;


use yii\base\Model;

class SignUpForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $passwordRepeat;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'filter', 'filter' => 'trim'],
            [['username', 'email'], 'required'],
            [['username', 'email'], 'string', 'max' => 255],

            ['email', 'email'],

            ['username', 'unique', 'targetClass' => '\app\models\User',
                'message' => 'This username has already been taken.'],
            ['email', 'unique', 'targetClass' => '\app\models\User',
                'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 4],

            ['passwordRepeat', 'compare', 'compareAttribute' => 'password',
                'skipOnEmpty' => false],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signUp()
    {
        if ($this->validate()) {
            $user = new User([
                'username' => $this->username,
                'email' => $this->email,
            ]);
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}