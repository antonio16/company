<?php

namespace app\models;

use Yii;
use yii\base\Model;

class GrantForm extends Model
{
    public $view;
    public $edit;
    public $groups;

    private $_companyId;
    private $_userId;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['view', 'edit'], 'boolean'],
            ['groups', 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'view' => 'Can View Articles',
            'edit' => 'Can Edit Articles',
            'groups' => 'Can View Items of Groups',
        ];
    }

    /**
     * @param $company_id
     * @param $user_id
     * @return GrantForm
     */
    public static function create($company_id, $user_id) {
        $model = new GrantForm();
        $model->_companyId = $company_id;
        $model->_userId = $user_id;

        $canView = ArticleViewer::findOne([
            'company_id' => $company_id,
            'user_id' => $user_id
        ]);

        $canEdit = ArticleEditor::findOne([
            'company_id' => $company_id,
            'user_id' => $user_id
        ]);

        $model->view = ($canView !== null);
        $model->edit = ($canEdit !== null);

        /**
         * @var $company Company
         */
        $company = Company::findOne($company_id);

        $groups = [];
        foreach ($company->groups as $group) {
            $itemViewer = ItemViewer::findOne([
                'group_id' => $group->id,
                'user_id' => $user_id
            ]);
            if ($itemViewer !== null) {
                $groups[] = $group->id;
            }
        }

        $model->groups = $groups;

        return $model;
    }

    public function grant() {
        $adminOwnerMaster = Yii::$app->authManager->checkAccess(
            $this->_userId,
            'viewCompany',
            ['company_id' => $this->_companyId]
        );

        if ($adminOwnerMaster) { return; }

        $this->_grantGroupPermissions();
        $this->_grantViewArticlesPermission();
        $this->_grantEditArticlesPermission();
    }

    private function _grantGroupPermissions() {
        $company_id = $this->_companyId;
        $user_id = $this->_userId;

        /**
         * @var $company Company
         */
        $company = Company::findOne($company_id);

        foreach ($company->groups as $group) {
            ItemViewer::deleteAll([
                'group_id' => $group->id,
                'user_id' => $user_id
            ]);

            if (!is_array($this->groups)) {
                continue;
            }

            if (in_array($group->id, $this->groups)) {
                $itemViewer = new ItemViewer([
                    'group_id' => $group->id,
                    'user_id' => $user_id
                ]);
                $itemViewer->save();
            }
        }
    }

    private function _grantViewArticlesPermission() {
        $company_id = $this->_companyId;
        $user_id = $this->_userId;

        ArticleViewer::deleteAll([
            'company_id' => $company_id,
            'user_id' => $user_id
        ]);

        if ($this->view) {
            $view = new ArticleViewer([
                'company_id' => $company_id,
                'user_id' => $user_id
            ]);
            $view->save();
        }
    }

    private function _grantEditArticlesPermission() {
        $company_id = $this->_companyId;
        $user_id = $this->_userId;

        ArticleEditor::deleteAll([
            'company_id' => $company_id,
            'user_id' => $user_id
        ]);

        if ($this->edit) {
            $edit = new ArticleEditor([
                'company_id' => $company_id,
                'user_id' => $user_id
            ]);
            $edit->save();
        }
    }
}