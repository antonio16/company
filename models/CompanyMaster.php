<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%company_master}}".
 *
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $created_at
 *
 * @property User $user
 * @property Company $company
 */
class CompanyMaster extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company_master}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'user_id'], 'required'],
            [['company_id', 'user_id', 'created_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {

            $adminOwnerMaster = Yii::$app->authManager->checkAccess(
                $this->user_id,
                'viewCompany',
                ['company_id' => $this->company_id]
            );

            if ($adminOwnerMaster) { return false; }

            ArticleViewer::deleteAll([
                'company_id' => $this->company_id,
                'user_id' => $this->user_id,
            ]);

            ArticleEditor::deleteAll([
                'company_id' => $this->company_id,
                'user_id' => $this->user_id,
            ]);

            /**
             * @var $company Company
             */
            $company = Company::findOne($this->company_id);
            foreach ($company->groups as $group) {
                ItemViewer::deleteAll([
                    'group_id' => $group->id,
                    'user_id' => $this->user_id,
                ]);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}