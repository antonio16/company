<?php

namespace app\rbac;

use app\models\CompanyMaster;
use Yii;
use yii\rbac\Rule;

class MasterRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'isMaster';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $company_id = $params['company_id'];

        /**
         * @var $companyMaster CompanyMaster
         */
        $companyMaster = CompanyMaster::findOne([
            'company_id' => $company_id,
            'user_id' => $user,
        ]);

        return ($companyMaster !== null);
    }
}