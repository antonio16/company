<?php

namespace app\rbac;

use app\models\Company;
use yii\rbac\Rule;

class OwnerRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'isOwner';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        /**
         * @var $company Company
         */
        $company = Company::findOne($params['company_id']);

        if ($company !== null) {
            return $company->owner_id == $user;
        }
        return false;
    }
}