<?php

namespace app\rbac;

use app\models\ArticleViewer;
use yii\rbac\Rule;

class ViewArticleRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'canViewArticles';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $viewer = ArticleViewer::findOne([
            'company_id' => $params['company_id'],
            'user_id' => $user
        ]);

        return ($viewer !== null);
    }
}