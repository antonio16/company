<?php

namespace app\rbac;


class AccessRule extends \yii\filters\AccessRule
{
    /**
     * @var array additional params that will be used
     * among with [[$this->roles]] in [[\yii\web\User::can()]]
     */
    public $params = [];

    /**
     * @inheritdoc
     */
    protected function matchRole($user)
    {
        if (empty($this->roles)) {
            return true;
        }
        foreach ($this->roles as $role) {
            if ($role === '?') {
                if ($user->getIsGuest()) {
                    return true;
                }
            } elseif ($role === '@') {
                if (!$user->getIsGuest()) {
                    return true;
                }
            } elseif ($user->can($role, $this->params)) {
                return true;
            }
        }
        return false;
    }
}