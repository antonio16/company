<?php

namespace app\rbac;

use app\models\ItemViewer;
use yii\rbac\Rule;

class ViewItemsRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'canViewItems';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $itemViewer = ItemViewer::findOne([
            'group_id' => $params['company_id'],
            'user_id' => $user
        ]);

        return ($itemViewer !== null);
    }
}