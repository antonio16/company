<?php

namespace app\rbac;

use app\models\ArticleEditor;
use yii\rbac\Rule;

class EditArticleRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'canEditArticles';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $editor = ArticleEditor::findOne([
            'company_id' => $params['company_id'],
            'user_id' => $user
        ]);

        return ($editor !== null);
    }
}