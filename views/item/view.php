<?php

/**
 * @var $item \app\models\Item
 */

$this->title = 'Write an Article';

$group = $item->group;

?>


<?php $this->beginContent('@app/views/layouts/company.php', [
    'company' => $group->company
]); ?>

<h2>Group: <?= $group->name ?></h2>

<h3>Item: <?= $item->name ?></h3>

<p class="text-info">If you see this page,
    it means that you have permissions to view items of this group.</p>

<a href="<?= Yii::$app->urlManager->createUrl(['item/index', 'id' => $group->id]) ?>"
   class="btn btn-default">Back</a>

<?php $this->endContent(); ?>
