<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $company \app\models\Company
 * @var $group \app\models\Group
 * @var $itemProvider \yii\data\ActiveDataProvider
 */

$urlManager = Yii::$app->urlManager;

$this->title = 'Items of Group ' . $group->name;

?>


<?php $this->beginContent('@app/views/layouts/company.php', [
    'company' => $group->company
]); ?>



<h3><?= Html::encode($this->title) ?></h3>

<?php

echo \yii\grid\GridView::widget([
    'dataProvider' => $itemProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        [
            'attribute' => 'created_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'attribute' => 'updated_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',
        ],
    ],
]);

?>

<a href="<?= $urlManager->createUrl(['item/create', 'id' => $group->id]) ?>"
   class="btn btn-info">Create Item</a>

<a href="<?= Yii::$app->urlManager->createUrl(['group/index', 'id' => $group->company_id]) ?>"
   class="btn btn-default">Back to Groups</a>

<?php $this->endContent(); ?>