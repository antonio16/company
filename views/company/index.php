<?php
/**
 * @var $companyProvider \yii\data\ActiveDataProvider
 */

$urlManager = Yii::$app->urlManager;
?>

<h1>Companies</h1>

<?php

echo \yii\grid\GridView::widget([
    'dataProvider' => $companyProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        [
            'attribute' => 'owner',
            'value' => function($model) {
                if ($model->owner_id == Yii::$app->user->id) {
                    $owner = "Me";
                } else {
                    $owner = $model->owner->username;
                }
                return $owner;
            }
        ],
        [
            'attribute' => 'created_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'attribute' => 'updated_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}'
        ],
    ],
]);

?>

<a href="<?= $urlManager->createUrl('/company/create') ?>"
   class="btn btn-default">Create Company</a>
