<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $company \app\models\Company
 * @var $groupProvider \yii\data\ActiveDataProvider
 */

$urlManager = Yii::$app->urlManager;

$this->title = 'Item Groups of the Company';

?>


<?php $this->beginContent('@app/views/layouts/company.php', [
    'company' => $company
]); ?>



<h3><?= Html::encode($this->title) ?></h3>

<?php

echo \yii\grid\GridView::widget([
    'dataProvider' => $groupProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        [
            'attribute' => 'created_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'attribute' => 'updated_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',
        ],
    ],
]);

?>

<a href="<?= $urlManager->createUrl(['group/create', 'id' => $company->id]) ?>"
   class="btn btn-default">Create Group</a>


<?php $this->endContent(); ?>