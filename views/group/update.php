<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $company \app\models\Company
 * @var $model \app\models\Group
 */

$this->title = 'Update Group';

?>


<?php $this->beginContent('@app/views/layouts/company.php', [
    'company' => $company
]); ?>

<h3><?= Html::encode($this->title) ?></h3>

<?php $form = ActiveForm::begin([
    'id' => 'group-update-form',
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>

<?= $form->field($model, 'name') ?>


<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>

        <a href="<?= Yii::$app->urlManager->createUrl(['group/index', 'id' => $company->id]) ?>"
           class="btn btn-default">Back</a>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->endContent(); ?>
