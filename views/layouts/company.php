<?php

use yii\bootstrap\Nav;

/**
 * @var $content string
 * @var $company \app\models\Company
 */

?>



<h1>Company <?= $company->name ?></h1>

<br>

<?php

echo Nav::widget([
    'items' => [
        [
            'label' => 'Overview',
            'url' => ['company/view', 'id' => $company->id],
        ],
        [
            'label' => 'Masters',
            'url' => ['master/index', 'id' => $company->id],
        ],
        [
            'label' => 'Articles',
            'url' => ['article/index', 'id' => $company->id],
        ],
        [
            'label' => 'Item Groups',
            'url' => ['group/index', 'id' => $company->id],
        ],
        [
            'label' => 'Privileges',
            'url' => ['privileges/index', 'id' => $company->id],
        ],

    ],
    'options' => [
        'class' =>'nav-pills',
        'style' => 'background-color: #f5f5f5;'
    ],
]);
?>

<br>

<?= $content ?>

