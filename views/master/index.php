<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $company \app\models\Company
 * @var $masterProvider \yii\data\ActiveDataProvider
 */

$urlManager = Yii::$app->urlManager;

$this->title = 'Masters of the Company';

?>


<?php $this->beginContent('@app/views/layouts/company.php', [
    'company' => $company
]); ?>



<h3><?= Html::encode($this->title) ?></h3>

<?php

echo \yii\grid\GridView::widget([
    'dataProvider' => $masterProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'username',
        [
            'attribute' => 'created_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function($url, $model) use ($company) {
                    $link =  Html::a(
                        '',
                        ['master/delete', 'id' => $company->id],
                        [
                            'class' => 'glyphicon glyphicon-remove-sign',
                            'title' => 'Remove master',
                            'data'=>[
                                'method' => 'post',
                                'params' => [
                                    'company_id' => $company->id,
                                    'user_id' => $model->id
                                ],
                            ]
                        ]
                    );

                    return $link;
                }
            ],
        ],
    ],
]);

?>

<a href="<?= $urlManager->createUrl(['master/add', 'id' => $company->id]) ?>"
   class="btn btn-default">Add Master</a>


<?php $this->endContent(); ?>