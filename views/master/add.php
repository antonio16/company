<?php
use yii\helpers\Html;

/**
 * @var $company \app\models\Company
 * @var $userProvider \yii\data\ActiveDataProvider
 */

$this->title = 'Add master to Company';

$authManager = Yii::$app->authManager;

?>


<?php $this->beginContent('@app/views/layouts/company.php', [
    'company' => $company
]); ?>

<h1><?= Html::encode($this->title) ?></h1>

<?php

echo \yii\grid\GridView::widget([
    'dataProvider' => $userProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'username',
        [
            'attribute' => 'created_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{add}',
            'buttons' => [
                'add' => function($url, $model) use ($company, $authManager) {
                    $link =  Html::a(
                        '',
                        ['master/add', 'id' => $company->id],
                        [
                            'class' => 'glyphicon glyphicon-plus-sign',
                            'title' => 'Add master',
                            'data'=>[
                                'method' => 'post',
                                'params' => [
                                    'company_id' => $company->id,
                                    'user_id' => $model->id
                                ],
                            ]
                        ]
                    );

                    $isAdmin = $authManager->checkAccess($model->id, 'admin');
                    $isOwner = $authManager->checkAccess($model->id, 'owner', [
                        'company_id' => $company->id
                    ]);
                    $isMaster = $authManager->checkAccess($model->id, 'master', [
                        'company_id' => $company->id
                    ]);

                    return (!$isAdmin && !$isOwner && !$isMaster) ? $link : '';
                    //return $link;
                }
            ],
        ],
    ],
]);

?>

<a href="<?= Yii::$app->urlManager->createUrl(['master/index', 'id' => $company->id]) ?>"
   class="btn btn-default">Back</a>

<?php $this->endContent(); ?>
