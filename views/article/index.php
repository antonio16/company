<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $company \app\models\Company
 * @var $articleProvider \yii\data\ActiveDataProvider
 */

$urlManager = Yii::$app->urlManager;

$this->title = 'Articles of the Company';

?>


<?php $this->beginContent('@app/views/layouts/company.php', [
    'company' => $company
]); ?>



<h3><?= Html::encode($this->title) ?></h3>

<?php

echo \yii\grid\GridView::widget([
    'dataProvider' => $articleProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        //'text',
        [
            'attribute' => 'created_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'attribute' => 'updated_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',
        ],
    ],
]);

?>

<a href="<?= $urlManager->createUrl(['article/create', 'id' => $company->id]) ?>"
   class="btn btn-default">Write an Article</a>


<?php $this->endContent(); ?>