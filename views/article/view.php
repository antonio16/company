<?php

/**
 * @var $company \app\models\Company
 * @var $article \app\models\Article
 */

$this->title = 'Write an Article';

?>


<?php $this->beginContent('@app/views/layouts/company.php', [
    'company' => $company
]); ?>

<h3><?= $article->name ?></h3>

<p><?= $article->text ?></p>

<p class="text-info">If you see this page,
    it means that you have permissions to view articles of this group.</p>

<a href="<?= Yii::$app->urlManager->createUrl(['article/index', 'id' => $company->id]) ?>"
   class="btn btn-default">Back</a>

<?php $this->endContent(); ?>
