<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $company \app\models\Company
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

$urlManager = Yii::$app->urlManager;
$authManager = Yii::$app->authManager;

$this->title = 'User Privileges';

?>


<?php $this->beginContent('@app/views/layouts/company.php', [
    'company' => $company
]); ?>



<h3><?= Html::encode($this->title) ?></h3>

<?php

echo \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'user_id',
            'header' => 'Username',
            'value' => function($model) {
                return $model->user->username;
            }
        ],
        [
            'header' => 'View Articles',
            'value' => function($model) use ($authManager, $company) {
                $canView = $authManager->checkAccess($model->user_id,
                    'viewArticles', ['company_id' => $company->id]);
                return ($canView ? 'Yes' : 'No');
            }
        ],
        [
            'header' => 'Edit Articles',
            'value' => function($model) use ($authManager, $company) {
                $canView = $authManager->checkAccess($model->user_id,
                    'editArticles', ['company_id' => $company->id]);
                return ($canView ? 'Yes' : 'No');
            }
        ],
        [
            'attribute' => 'created_at',
            'format' => ['date', 'php:Y-m-d H:i']
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{grant}',
            'buttons' => [
                'grant' => function($url, $model) use ($company) {
                    $link =  Html::a(
                        '',
                        ['privileges/grant', 'id' => $company->id,
                            'user_id' => $model->user_id],
                        [
                            'class' => 'glyphicon glyphicon-pencil',
                            'title' => 'Change',
                        ]
                    );

                    return $link;
                }
            ],
        ],
    ],
]);

?>

<a href="<?= $urlManager->createUrl(['privileges/find', 'id' => $company->id]) ?>"
   class="btn btn-default">Grant New</a>


<?php $this->endContent(); ?>